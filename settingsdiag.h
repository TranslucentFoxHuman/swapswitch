#ifndef SETTINGSDIAG_H
#define SETTINGSDIAG_H

#include <QDialog>

namespace Ui {
class SettingsDiag;
}

class SettingsDiag : public QDialog
{
    Q_OBJECT

public:
    explicit SettingsDiag(QWidget *parent = nullptr);
    ~SettingsDiag();

private slots:
    void on_pushButton_clicked();

private:
    Ui::SettingsDiag *ui;
};

#endif // SETTINGSDIAG_H
