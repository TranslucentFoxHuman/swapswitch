#include "settingsdiag.h"
#include "ui_settingsdiag.h"
#include "mainwindow.h"

SettingsDiag::SettingsDiag(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::SettingsDiag)
{
    MainWindow *mwin = new MainWindow(this);
    ui->setupUi(this);

    ui->lineEdit->setText(QString::fromStdString(mwin->get_swapdev()));
}

SettingsDiag::~SettingsDiag()
{
    delete ui;
}

void SettingsDiag::on_pushButton_clicked()
{

    MainWindow *mwin = new MainWindow(this);
    mwin->swapdev_update(ui->lineEdit->text().toStdString());
    mwin->configupdate();
    close();
}
