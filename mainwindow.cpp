#include "mainwindow.h"
#include "ui_mainwindow.h"
#include "settingsdiag.h"
#include <unistd.h>
#include <sys/types.h>
#include <stdlib.h>
#include <QMessageBox>
#include <pwd.h>
#include <iostream>
#include <fstream>
#include <string>
#include <QDebug>
#include "aboutdialog.h"

//#define __DEBUG__

std::string swapdev;

std::string *fdata = new std::string[4096];

struct passwd *pw = getpwuid(getuid());

std::string homedir = pw->pw_dir;

int swapdevline;

std::string configpath;

int testint = 0;

void MainWindow::swapdev_update(std::string arg){
    swapdev = arg;
}

std::string MainWindow::get_swapdev(){
    return swapdev;
}

void MainWindow::configupdate(void) {
    fdata[swapdevline] = "SWAPDEV=" + swapdev;
    std::ofstream ofstrm(configpath);
    std::string fulldata;
    int i = 0;
    while (1){
        if(fdata[i] == ""){
            break;
        }
        if(i >= 4096) {
            break;
        }
        fulldata = fulldata + fdata[i] + "\n";
        i++;
    }
    ofstrm << fulldata;
}

void show_settings(void) {

}

MainWindow::MainWindow(QWidget *parent)
    : QMainWindow(parent)
    , ui(new Ui::MainWindow)
{
#ifndef __DEBUG__
    if (0 != getuid()) {
        QMessageBox msgbox;
        msgbox.setIcon(QMessageBox::Critical);
        msgbox.setText("This application needs to be run with the root account.");
        msgbox.setStandardButtons(QMessageBox::Ok);
        msgbox.setDefaultButton(QMessageBox::Ok);
        msgbox.exec();
        exit(1);
    }
#endif

    configpath = homedir+"/.swapswitch.cfg";

        std::ifstream ifstrm(configpath);
        if (!ifstrm) {
             ifstrm.close();
             std::ofstream ofstrm(configpath);
             ofstrm << "SWAPDEV=";
             ofstrm.close();
        } else {
            ifstrm.close();
        }
        ifstrm.open(configpath);

        int rline = 0;
            while (!ifstrm.eof())	{
                if (rline >= 4096) {
                    std::cout << "Configuration file is too big to open!\n";
                    exit(3);
                }
                std::getline(ifstrm,fdata[rline]);
                rline++;
        }

            ifstrm.close();

            int pline = 0;
                while(1) {
                    if (pline > rline) {
                        std::cout << "Configuration file has an Error\n";
                        exit(3);
                    }
                    int fres = fdata[pline].find("SWAPDEV");
                    if (fres == std::string::npos) {
                        pline++;
                        continue;
                    }  else {
                        fres = fdata[pline].find("=")  +1;
                        swapdev = fdata[pline].substr(fres);
                        swapdevline = pline;
                        break;
                    }
                    pline++;
                }

    ui->setupUi(this);
}

MainWindow::~MainWindow()
{
    delete ui;
}


void MainWindow::on_enableButton_clicked()
{
    std::string cmd = "swapon " + swapdev;
#ifndef __DEBUG__
    system(cmd.c_str());
#endif
#ifdef __DEBUG__
    qDebug(cmd.c_str());
#endif
}

void MainWindow::on_actionSettings_triggered(){
    SettingsDiag *stdiag = new SettingsDiag(this);
    stdiag->show();
}

void MainWindow::on_disableButton_clicked()
{
    std::string cmd = "swapoff " + swapdev;
#ifndef __DEBUG__
    system(cmd.c_str());
#endif
#ifdef __DEBUG__
    qDebug(cmd.c_str());
#endif
}

void MainWindow::on_actionAbout_triggered(){
    AboutDialog *abt = new AboutDialog(this);
    abt->show();
}
