#ifndef MAINWINDOW_H
#define MAINWINDOW_H

#include <QMainWindow>

QT_BEGIN_NAMESPACE
namespace Ui { class MainWindow; }
QT_END_NAMESPACE

class MainWindow : public QMainWindow
{
    Q_OBJECT

public:
    MainWindow(QWidget *parent = nullptr);
    ~MainWindow();
    void swapdev_update(std::string arg);
    void configupdate(void);
    std::string get_swapdev(void);

private slots:
    void on_enableButton_clicked();
    void on_actionSettings_triggered();
    void on_actionAbout_triggered();

    void on_disableButton_clicked();

private:
    Ui::MainWindow *ui;
};
#endif // MAINWINDOW_H
